# LIS4381

## Alexander Barlow

### Assignment 1 Requirements:

1. Distributed Version Control with Git and BitBucket
2. AMPPS Installation
3. Questions
4. Entity Relation Diagram and SQL Code (Optional)
5. BitBucket Distributed Links
	a) this assignment and
	b) the completed tutorial (bitbucketstationlocations)

>Business Rules
>
>The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the 
following employee data for tax purposes:job description, length of employment, benefits,number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories mustbe tracked. Also, include the following business rules:
>
>* Each employee may have one or more dependents.• Each employee has only one job.
>
>* Each job can be held by many employees.
>
>* Many employees may receive many benefits.
>
>* Many benefits may be selected by many employees(though, while they may not select any benefits—any dependents of employees may be on anemployee’s plan).
>
>
>Notes:
>
>* Employee/Dependent tables must use suitable attributes (See Assignment Guidelines);
>
>
>In Addition:
>
>* Employee: SSN, DOB, start/end dates, salary;
>
>* Dependent: same information as their associated employee(though, not start/end dates),date added(as dependent),type of relationship: e.g., father, mother, etc.
>
>* Job: title(e.g., secretary, service tech., manager, cashier, janitor, IT, etc.)
>
>* Benefit: name(e.g., medical, dental, long-term disability, 401k, term life insurance, etc.)
>
>* Plan: type(single, spouse, family), cost, election date(plans must be unique)
>
>* Employee history: jobs, salaries, and benefit changes, as well as who made the change and why;
>
>* Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ);
>
>* *All* tables must include notes attribute.

#### README.md should include the following item(s):

* Screenshot of AMPPS running
* Screenshot of A1 ERD
* Ex.1 SQL Solution
* git commands w/short descriptions

> #### Git commands w/short descriptions:

1. git init - creates a new empty Git repository or initilizes an existing one 
2. git status - displays the state of the working directory and staging area 
3. git add - adds a change from working directory to staging area 
4. git commit - saves changes to local respository
5. git push -  uploads local repository to remote repository
6. git pull - pulls files and changes from remote repository to local repository
7. git init --help - provides a synopsis of the most commonly used commands

#### Assignment Screenshots:

*Screenshot of AMPPS running*:

![A1 AMPPS Screenshot](img/ampps.png)

*Screenshot of A1 ERD*:

![A1 ERD Screenshot](img/a1erd.png)

*Screenshot of A1 Ex1 SQL Solution*:

![A1 Ex1 SQL Solution Screenshot](img/solution.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket Station Locations")
