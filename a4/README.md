# LIS4381

## Alexander Barlow

### Assignment 4 Requirements:

1. Tables and Insert Statements made in MS SQL
2. Include Indexes and Foreign Key SQL
3. Query Results sets
4. Five records per table, with ten records in Person
5. Questions
6. Screenshot


#### README.md should include the following item(s):

* Screenshot of ERD.


#### Assignment Screenshots:

*Screenshot of A4 ERD*:

![A4 ER Diagram Screenshot](img/a4erd.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket Station Locations")
