# LIS4381

## Alexander Barlow

### Project 2 Requirements:

1. Setup and use a new cluster with Mongodb Atlus
2. Import a new database with the command line for MongoDB
3. Perform queries
4. Questions
5. Screenshot


#### Assignment Screenshot:

*Screenshot of P2 MongoDB Shell Command*:

![P2 MongoDb Shell Command Screenshot](img/screenshot1.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket Station Locations")
