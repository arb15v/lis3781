# LIS4381

## Alexander Barlow

### Assignment 4 Requirements:

1. Add the sale, time, city, state, and region tables
2. Include Indexes and Foreign Key SQL
3. Query Results sets
4. Five records per table, with ten records in Person and twenty-five records in sale
5. Questions
6. Screenshot


#### README.md should include the following item(s):

* Screenshot of ERD.


#### Assignment Screenshots:

*Screenshot of A5 ERD*:

![A5 ER Diagram Screenshot](img/a5erd.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket Station Locations")
