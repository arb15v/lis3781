IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'arb15v')
DROP DATABASE arb15v;
GO

IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'arb15v')
CREATE DATABASE arb15v;
GO

use arb15v;
GO

-- ----------------------------------------------------------------
-- Table person
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.person', N'U') IS NOT NULL
DROP TABLE dbo.person
GO

CREATE TABLE dbo.person
(
    per_id SMALLINT not null identity(1,1),
    per_ssn BINARY(64) NULL,
    per_salt BINARY(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_gender CHAR(1) not null check (per_gender IN('m','f')),
    per_dob DATE NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL,
    per_zip INT not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email VARCHAR(100) NOT NULL,
    per_type CHAR(1) NOT NULL check (per_type IN('c','s')),
    per_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT ux_per_ssn unique nonclustered (per_ssn ASC)
);

-- ----------------------------------------------------------------
-- Table phone
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone
GO

CREATE TABLE dbo.phone
(
    phn_id SMALLINT not null identity(1,1),
    per_id SMALLINT not null,
    phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) not null check (phn_type IN('h','c','w','f')),
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    CONSTRAINT fk_phone_person
     FOREIGN KEY (per_id)
     REFERENCES dbo.person (per_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE

);

-- ----------------------------------------------------------------
-- Table customer
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.customer', N'U') IS NOT NULL
DROP TABLE dbo.customer
GO

CREATE TABLE dbo.customer
(
    per_id SMALLINT not null,
    cus_balance decimal(7,2) not null check (cus_balance >= 0),
    cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_customer_person
     FOREIGN KEY (per_id)
     REFERENCES dbo.person (per_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table slsrep
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep
GO

CREATE TABLE dbo.slsrep
(
    per_id SMALLINT not null,
    srp_yr_sales_goal decimal (8,2) not null check (srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
    srp_notes VARCHAR(255) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_slsrep_person
     FOREIGN KEY (per_id)
     REFERENCES dbo.person (per_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table srp_hist
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.srp_hist', N'U') IS NOT NULL
DROP TABLE dbo.srp_hist
GO

CREATE TABLE dbo.srp_hist
(
    sht_id SMALLINT not null identity(1,1),
    per_id SMALLINT not null,
    sht_type char(1) not null check (sht_type IN('i','u','d')),
    sht_modified date not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
    sht_yr_total_sales decimal(8,2) not null check (sht_yr_total_sales >= 0),
    sht_yr_total_comm decimal(7,2) not null check (sht_yr_total_comm >= 0),
    sht_notes VARCHAR(255) NULL,
    PRIMARY KEY (sht_id),

    CONSTRAINT fk_srp_hist_slsrep
     FOREIGN KEY (per_id)
     REFERENCES dbo.slsrep (per_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table contact
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.contact', N'U') IS NOT NULL
DROP TABLE dbo.contact
GO

CREATE TABLE dbo.contact
(
    cnt_id INT not null identity(1,1),
    per_cid SMALLINT not null,
    per_sid smallint not null,
    cnt_date datetime not null,
    cnt_notes varchar(255) null,
    PRIMARY KEY (cnt_id),

    CONSTRAINT fk_contact_customer
     FOREIGN KEY (per_cid)
     REFERENCES dbo.customer (per_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE,

    CONSTRAINT fk_contact_slsrep
     FOREIGN KEY (per_sid)
     REFERENCES dbo.slsrep (per_id)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
);

-- ----------------------------------------------------------------
-- Table [order]
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order]
GO

CREATE TABLE dbo.[order]
(
    ord_id INT not null identity(1,1),
    cnt_id INT not null,
    ord_placed_date datetime not null,
    ord_filled_date DATETIME null,
    ord_notes varchar(255) null,
    PRIMARY KEY (ord_id),

    CONSTRAINT fk_order_contact
     FOREIGN KEY (cnt_id)
     REFERENCES dbo.contact (cnt_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table region
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.region', N'U') IS NOT NULL
DROP TABLE dbo.region
GO

CREATE TABLE dbo.region
(
    reg_id tinyint not null identity(1,1),
    reg_name char(1) not null,
    reg_notes varchar(255) null,
    PRIMARY KEY (reg_id)
);

-- ----------------------------------------------------------------
-- Table state
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.state', N'U') IS NOT NULL
DROP TABLE dbo.state
GO

CREATE TABLE dbo.state
(
    ste_id tinyint not null identity(1,1),
    reg_id tinyint not null,
    ste_name char(2) not null default 'FL',
    ste_notes varchar(255) null,
    PRIMARY KEY (ste_id),

    CONSTRAINT fk_state_region
     FOREIGN KEY (reg_id)
     REFERENCES dbo.region (reg_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table city
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.city', N'U') IS NOT NULL
DROP TABLE dbo.city
GO

CREATE TABLE dbo.city
(
    cty_id smallint not null identity(1,1),
    ste_id tinyint not null,
    cty_name varchar(30) not null,
    cty_notes varchar(255) null,
    PRIMARY KEY (cty_id),

    CONSTRAINT fk_city_state
     FOREIGN KEY (ste_id)
     REFERENCES dbo.state (ste_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table store
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store
GO

CREATE TABLE dbo.store
(
    str_id smallint not null identity(1,1),
    cty_id smallint not null,
    str_name varchar(45) not null,
    str_street VARCHAR(30) NOT NULL,
    str_zip INT not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email VARCHAR(100) NOT NULL,
    str_url varchar(100) not null,
    str_notes varchar(255) null,
    PRIMARY KEY (str_id),

    CONSTRAINT fk_store_city
     FOREIGN KEY (cty_id)
     REFERENCES dbo.city (cty_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table invoice
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice
GO

CREATE TABLE dbo.invoice
(
    inv_id int not null identity(1,1),
    ord_id int not null,
    str_id smallint not null,
    inv_date datetime not null,
    inv_total decimal(8,2) not null check (inv_total >= 0),
    inv_paid bit not null,
    inv_notes varchar(255) null,
    PRIMARY KEY (inv_id),

    CONSTRAINT ux_ord_id unique nonclustered (ord_id ASC),

    CONSTRAINT fk_invoice_order
     FOREIGN KEY (ord_id)
     REFERENCES dbo.[order] (ord_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE,

    CONSTRAINT fk_invoice_store
     FOREIGN KEY (str_id)
     REFERENCES dbo.store (str_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table payment
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment
GO

CREATE TABLE dbo.payment
(
    pay_id int not null identity(1,1),
    inv_id int not null,
    pay_date datetime not null,
    pay_amt decimal(7,2) not null check (pay_amt >= 0),
    pay_notes varchar(255) null,
    PRIMARY KEY (pay_id),

    CONSTRAINT fk_payment_invoice
     FOREIGN KEY (inv_id)
     REFERENCES dbo.invoice (inv_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table vendor
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.vendor', N'U') IS NOT NULL
DROP TABLE dbo.vendor
GO

CREATE TABLE dbo.vendor
(
    ven_id smallint not null identity(1,1),
    ven_name varchar(45) not null,
    ven_street VARCHAR(30) NOT NULL,
    ven_city VARCHAR(30) NOT NULL,
    ven_state CHAR(2) NOT NULL,
    ven_zip INT not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email VARCHAR(100) NULL,
    ven_url varchar(100) null,
    ven_notes varchar(255) null,
    PRIMARY KEY (ven_id),
);

-- ----------------------------------------------------------------
-- Table product
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.product', N'U') IS NOT NULL
DROP TABLE dbo.product
GO

CREATE TABLE dbo.product
(
    pro_id smallint not null identity(1,1),
    ven_id smallint not null,
    pro_name varchar(30)  not null,
    pro_descript varchar(45) null,
    pro_weight float not null check (pro_weight >= 0),
    pro_qoh smallint not null check (pro_qoh >= 0),
    pro_cost decimal(7,2) not null check (pro_cost >= 0),
    pro_price decimal(7,2) not null check (pro_price >= 0),
    pro_discount decimal(3,0) null,
    pro_notes varchar(255) null,
    PRIMARY KEY (pro_id),

    CONSTRAINT fk_product_vendor
     FOREIGN KEY (ven_id)
     REFERENCES dbo.vendor (ven_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table product_hist
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.product_hist', N'U') IS NOT NULL
DROP TABLE dbo.product_hist
GO

CREATE TABLE dbo.product_hist
(
    pht_id int not null identity(1,1),
    pro_id smallint not null,
    pht_date datetime not null,
    pht_cost decimal(7,2) not null check (pht_cost >= 0),
    pht_price decimal(7,2) not null check (pht_price >= 0),
    pht_discount decimal(3,0) null,
    pht_notes varchar(255) null,
    PRIMARY KEY (pro_id),

    CONSTRAINT fk_product_hist_product
     FOREIGN KEY (pro_id)
     REFERENCES dbo.product (pro_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table orderline
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.orderline', N'U') IS NOT NULL
DROP TABLE dbo.orderline
GO

CREATE TABLE dbo.orderline
(
    oln_id int not null identity(1,1),
    ord_id int not null,
    pro_id smallint not null,
    oln_qty smallint not null check (oln_qty >= 0),
    oln_price decimal(7,2) not null check (oln_price >= 0),
    oln_notes varchar(255) null,
    PRIMARY KEY (oln_id),

    CONSTRAINT fk_ordeline_order
     FOREIGN KEY (ord_id)
     REFERENCES dbo.[order] (ord_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE,

    CONSTRAINT fk_ordeline_product
     FOREIGN KEY (pro_id)
     REFERENCES dbo.product (pro_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);

-- ----------------------------------------------------------------
-- Table time
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.time', N'U') IS NOT NULL
DROP TABLE dbo.time
GO

CREATE TABLE dbo.time
(
    tim_id int not null identity(1,1),
    tim_yr smallint not null,
    tim_qtr tinyint not null,
    tim_month tinyint not null,
    tim_week tinyint not null,
    tim_day tinyint not null,
    tim_time time not null,
    tim_notes varchar(255) null,
    PRIMARY KEY (tim_id)
);

-- ----------------------------------------------------------------
-- Table sale
-- ----------------------------------------------------------------
IF OBJECT_ID(N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale
GO

CREATE TABLE dbo.sale
(
    pro_id smallint not null,
    str_id smallint not null,
    cnt_id int not null,
    tim_id int not null,
    sal_qty smallint not null,
    sal_price decimal(8,2) not null,
    sal_total decimal(8,2) not null,
    sal_notes varchar(255) null,
    PRIMARY KEY (pro_id, cnt_id, tim_id, str_id),

    CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
    unique nonclustered (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),

    CONSTRAINT fk_sale_time
     FOREIGN KEY (tim_id)
     REFERENCES dbo.time (tim_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE,

    CONSTRAINT fk_sale_contact
     FOREIGN KEY (cnt_id)
     REFERENCES dbo.contact (cnt_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE,
    
    CONSTRAINT fk_sale_store
     FOREIGN KEY (str_id)
     REFERENCES dbo.store (str_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE,
    
    CONSTRAINT fk_sale_product
     FOREIGN KEY (pro_id)
     REFERENCES dbo.product (pro_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE
);


select * from information_schema.tables;

select HASHBYTES('SHA2_512', 'test');
select len(HASHBYTES('SHA2_512', 'test'));

-- ----------------------------------------------------------------
-- data person
-- ----------------------------------------------------------------
INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1,NULL,'Steve','Rogers','m','1923-10-03','437 Southern Drive','Rochester','NY',324402222,'srogers@comcast.net','s',NULL),
(2,NULL,'Bruce','Wayne','m','1968-03-02','1007  Mountain Drive','Gotham','NY',983208440,'bwayne@knology.net','s',NULL),
(3,NULL,'Peter','Parker','m','1988-09-12','20 Ingram Street','New York','NY',102862341,'pparker@msn.com','s',NULL),
(4,NULL,'Jane','Thompson','f','1978-05-08','13563 Ocean View Drive','Seattle','WA',132084409,'jthompson@gmail.com','s',NULL),
(5,NULL,'Debra','Steele','f','1994-07-19','543 Oak Ln.','Milwuakee','WI',286234178,'dsteel@verizon.net','s',NULL),
(6,NULL,'Tony','Stark','m','1972-05-04','332 Palm Ave.','Malibu','CA',902638332,'tstark@yahoo.com','c',NULL),
(7,NULL,'Hank','Pym','m','1980-08-28','2355 Brown Street','Cleveland','OH',822348890,'hpym@aol.com','c',NULL),
(8,NULL,'Bob','Best','m','1992-02-10','4902 Avendale Avenue','Scottsdale','AZ',872638332,'bbest@yahoo.com','c',NULL),
(9,NULL,'Sandra','Dole','f','1990-01-26','87912 Lawrence Ave.','Atlanta','GA',672348890,'sdole@gmail.com','c',NULL),
(10,NULL,'Ben','Avery','m','1983-12-24','6432 Thunderbird Ln.','Souix Falls','SD',562638332,'bavery@hotmail.com','c',NULL),
(11,NULL,'Arthur','Curry','m','1975-12-15','3304 Euclid Avenue','Miami','FL',342219932,'acurry@gmail.com','c',NULL),
(12,NULL,'Diana','Price','f','1980-08-22','944 Green Street','Las Vegas','NV',332048823,'dprice@sumpatico.com','c',NULL),
(13,NULL,'Adam','Jurris','m','1995-01-31','98435 Valencia Dr.','Gulf Shores','AL',870219932,'ajurris@gmx.com','c',NULL),
(14,NULL,'Judy','Sleen','f','1970-03-22','56343 Rover Ct.','Billings','MT',672048823,'jsleen@sympatico.com','c',NULL),
(15,NULL,'Bill','Neiderheim','m','1982-03-13','43567 Netherland Blvd.','South Bend','IN',320219932,'bneiderheim@comcast.net','c',NULL);
GO

-- create le hashes
CREATE PROCEDURE dbo.CreatePersonSSN2
AS
BEGIN

    DECLARE @salt binary(64);
    DECLARE @ran_num int;
    DECLARE @ssn binary(64);
    DECLARE @x INT, @y INT;
    SET @x =1;

    SET @y=(select count(*) from dbo.person);

        WHILE (@x <= @y)
        BEGIN
        SET @salt=CRYPT_GEN_RANDOM(64);
        SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
        SET @ssn=HASHBYTES('SHA2_512', concat(@salt, @ran_num));

        update dbo.person
        set per_ssn=@ssn, per_salt=@salt
        where per_id=@x;

        SET @x = @x + 1;
        END;
END;
GO

exec dbo.CreatePersonSSN2

select * from dbo.person;

-- ----------------------------------------------------------------
-- data slsrep
-- ----------------------------------------------------------------
INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1,100000,60000,1800,NULL),
(2,80000,35000,3500,NULL),
(3,150000,84000,9650,'great salesperson'),
(4,125000,87000,15300,NULL),
(5,98000,43000,8750,NULL);

select * from dbo.slsrep;

-- ----------------------------------------------------------------
-- data customer
-- ----------------------------------------------------------------
INSERT INTO dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES
(6,120,14789,NULL),
(7,98.46,234.92,NULL),
(8,0,4578,NULL),
(9,981.73,1672.38,NULL),
(10,541.23,782.57,NULL);

select * from dbo.customer;

-- ----------------------------------------------------------------
-- data contact
-- ----------------------------------------------------------------
INSERT INTO dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES
(1,6,'1999-01-01',NULL),
(2,6,'2001-09-29',NULL),
(3,7,'2002-08-15',NULL),
(2,7,'2002-09-01',NULL),
(4,7,'2004-01-05',NULL);

select * from dbo.contact;

-- ----------------------------------------------------------------
-- data order
-- ----------------------------------------------------------------
INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1,'2010-11-23','2010-12-24',NULL),
(2,'2005-03-19','2005-07-28',NULL),
(3,'2011-07-01','2011-07-06',NULL),
(4,'2009-12-24','2010-01-05',NULL),
(5,'2008-09-21','2008-11-26',NULL);

select * from dbo.[order];

-- ----------------------------------------------------------------
-- data region
-- ----------------------------------------------------------------
INSERT INTO dbo.region
(reg_name, reg_notes)
VALUES
('c',NULL),
('n',NULL),
('e',NULL),
('s',NULL),
('w',NULL);

select * from dbo.region;

-- ----------------------------------------------------------------
-- data state
-- ----------------------------------------------------------------
INSERT INTO dbo.state
(reg_id, ste_name, ste_notes)
VALUES
(1,'MI',NULL),
(3,'IL',NULL),
(4,'WA',NULL),
(5,'FL',NULL),
(2,'TX',NULL);

select * from dbo.state;

-- ----------------------------------------------------------------
-- data city
-- ----------------------------------------------------------------
INSERT INTO dbo.city
(ste_id, cty_name, cty_notes)
VALUES
(1,'Detroit',NULL),
(2,'Aspen',NULL),
(2,'Chicago',NULL),
(3,'Clover',NULL),
(4,'St. Louis',NULL);

select * from dbo.city;

-- ----------------------------------------------------------------
-- data store
-- ----------------------------------------------------------------
INSERT INTO dbo.store
(cty_id, str_name, str_street, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
(2,'Walgreens','14567 Walnut Ln','475315690','3127658127','info@walgreens.com','http://www.walgreens.com',NULL),
(3,'CVS','572 Casper Road','475315691','3127658128','help@cvs.com','http://www.cvs.com',NULL),
(4,'Lowes','81309 Catapult Ave','475315692','3127658129','sales@lowes.com','http://www.lowes.com',NULL),
(5,'Walmart','14568 Walnut Ln','475315693','3127658120','info@walmart.com','http://www.walmart.com',NULL),
(1,'Dollar General','47583 Davison Rd','475315694','3127658121','ask@dollargeneral.com','http://www.dollargeneral.com',NULL);

select * from dbo.store;

-- ----------------------------------------------------------------
-- data invoice
-- ----------------------------------------------------------------
INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES
(5,4,'2001-05-03',58.32,0,NULL),
(4,5,'2006-11-11',100.59,0,NULL),
(1,1,'2010-09-16',57.34,0,NULL),
(3,2,'2011-01-10',99.32,1,NULL),
(2,3,'2008-06-24',1109.67,1,NULL);

select * from dbo.invoice;

-- ----------------------------------------------------------------
-- data vendor
-- ----------------------------------------------------------------
INSERT INTO dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES
('Sysco','531 Dolphin Run','Orlando','FL','344761234','7641238543','sales@sysco.com','https://www.sysco.com',NULL),
('General Electric','100 Happy Trails Dr','Boston','MA','344761235','7641238544','support@ge.com','https://www.ge.com',NULL),
('Cisco','300 Cisco Road','Stanford','OR','344761236','7641238545','cisco@cisco.com','https://www.cisco.com',NULL),
('Goodyear','100 Goodyear Dr.','Gary','IN','344761237','7641238546','sales@goodyear.com','https://www.goodyear.com',NULL),
('Snap-On','42185 Magenta Ave','Lake Falls','ND','344761238','7641238547','support@snapon.com','https://www.snap-on.com',NULL);

select * from dbo.vendor;

-- ----------------------------------------------------------------
-- data product
-- ----------------------------------------------------------------
INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(1,'hammer','',2.5,45,4.99,7.99,30,NULL),
(2,'screwdriver','',1.8,120,1.99,3.49,NULL,NULL),
(4,'pail','16 gallon',2.8,48,3.89,7.99,40,NULL),
(5,'cooking oil','peanut oil',15,19,19.99,28.99,NULL,NULL),
(3,'frying pan','',3.5,178,8.49,13.99,50,NULL);

select * from dbo.product;

-- ----------------------------------------------------------------
-- data orderline
-- ----------------------------------------------------------------
INSERT INTO dbo.orderline
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
VALUES
(1,2,10,8.0,NULL),
(2,3,7,9.88,NULL),
(3,4,3,6.99,NULL),
(5,1,2,12.76,NULL),
(4,5,13,58.99,NULL);

select * from dbo.orderline;

-- ----------------------------------------------------------------
-- data payment
-- ----------------------------------------------------------------
INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(5,'2008-07-01',5.99,NULL),
(4,'2010-09-28',4.99,NULL),
(1,'2008-07-23',8.75,NULL),
(3,'2010-10-31',19.55,NULL),
(2,'2011-03-29',32.5,NULL);

select * from dbo.payment;

-- ----------------------------------------------------------------
-- data product_hist
-- ----------------------------------------------------------------
INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1,'2005-01-02 11:53:34',4.99,7.99,30,NULL),
(2,'2005-02-03 09:13:56',1.99,3.49,NULL,NULL),
(3,'2006-03-04 23:21:49',3.89,7.99,40,NULL),
(4,'2006-05-06 18:09:04',19.99,28.99,NULL,NULL),
(5,'2006-05-07 15:07:29',8.49,13.99,50,NULL);

select * from dbo.product_hist;

-- ----------------------------------------------------------------
-- data time
-- ----------------------------------------------------------------
INSERT INTO dbo.time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
VALUES
(2008,2,5,19,7,'11:59:59',NULL),
(2010,4,12,49,4,'08:34:21',NULL),
(1999,4,12,52,5,'05:21:34',NULL),
(2011,3,8,36,1,'09:32:18',NULL),
(2001,1,7,27,2,'23:56:32',NULL);


select * from dbo.time;

-- ----------------------------------------------------------------
-- data sale
-- ----------------------------------------------------------------
INSERT INTO dbo.sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
VALUES
(1,5,5,3,20,9.99,199.8,NULL),
(2,4,4,2,5,5.99,29.95,NULL),
(3,4,5,1,30,3.99,119.7,NULL),
(4,2,2,5,15,18.99,284.85,NULL),
(5,1,1,4,6,11.99,71.94,NULL),
(5,2,1,1,10,9.99,199.8,NULL),
(4,3,2,2,5,5.99,29.95,NULL),
(3,1,3,1,30,3.99,119.7,NULL),
(2,3,4,4,15,18.99,284.85,NULL),
(1,4,5,5,6,11.99,199.8,NULL),
(1,2,5,1,10,11.99,29.95,NULL),
(2,3,4,2,20,9.99,119.7,NULL),
(3,4,3,3,19,5.99,284.85,NULL),
(4,5,2,4,23,3.99,71.94,NULL),
(5,4,1,5,12,18.99,29.95,NULL),
(5,3,1,1,25,11.99,119.7,NULL),
(4,2,2,2,28,9.99,284.85,NULL),
(3,1,3,3,10,5.99,71.94,NULL),
(2,1,4,4,6,3.99,199.8,NULL),
(1,2,5,5,2,18.99,29.95,NULL),
(1,3,5,5,6,11.99,119.7,NULL),
(2,4,4,4,18,9.99,284.85,NULL),
(3,5,3,3,30,5.99,71.94,NULL),
(4,4,2,2,12,3.99,199.8,NULL),
(5,2,3,1,13,18.99,29.95,NULL);

select * from dbo.sale;

-- ----------------------------------------------------------------
-- data srp_hist
-- ----------------------------------------------------------------
INSERT INTO dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm,sht_notes)
VALUES
(1,'i',getDate(),SYSTEM_USER,getDate(),100000,110000,11000,NULL),
(4,'i',getDate(),SYSTEM_USER,getDate(),150000,175000,17500,NULL),
(3,'u',getDate(),SYSTEM_USER,getDate(),200000,185000,18500,NULL),
(2,'u',getDate(),ORIGINAL_LOGIN(),getDate(),210000,220000,22000,NULL),
(5,'i',getDate(),ORIGINAL_LOGIN(),getDate(),225000,230000,23000,NULL);

select * from dbo.srp_hist;

-- ----------------------------------------------------------------
-- data phone
-- ----------------------------------------------------------------
INSERT INTO dbo.phone
(per_id, phn_num, phn_type, phn_notes)
VALUES
(1,'6451238756','h',NULL),
(3,'6451238757','c',NULL),
(5,'6451238758','w',NULL),
(7,'6451238759','f',NULL),
(9,'6451238750','h',NULL);

select * from dbo.phone;




/*
1. c
2. b
3. c
4. b
5. c
6. a
7. d
8. c
9. b
10. b
11. b
12. b
13. c
14. b
15. b
16. b
17. a
18. c
19. c
20. b
21. a
22. e
23. d
24. c
*/


