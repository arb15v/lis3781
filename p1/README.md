# LIS4381

## Alexander Barlow

### Project 1 Requirements:

1. Tables and Insert Statements
2. Include Indexes and Foreign Key SQL
3. Query Results sets
4. Five records per table, with Client, Attorney, and Judge having certain primary key values
5. Questions
6. Screenshots


#### README.md should include the following item(s):

* Screenshot of ERD.


#### Assignment Screenshots:

*Screenshot of Customer Table Creation*:

![P1 ER Diagram Screenshot](img/p1erd.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket Station Locations")
